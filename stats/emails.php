<?
############################################################################################
########################## CONNECT TO DB ###################################################
############################################################################################
error_reporting(E_ALL);
require_once('../conf.inc');


try {
    $conn = new PDO($dsn, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

############################################################################################
?>
<!doctype html>

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
  <meta charset="utf-8">

  <title>Profielen | Leerstijlentest</title>

  <meta name="description" content="Het onafhankelijk blad en site van de Hogeschool Rotterdam" />
  <meta name="author" content="Profielen" />

  <!-- http://t.co/dKP3o1e -->
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1" />

  <!-- For all browsers -->
  <link href="http://profielen.hr.nl/leerstijlentest/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <link href="http://profielen.hr.nl/leerstijlentest/css/print.css" media="print" rel="stylesheet" type="text/css" />

  <!-- IE9 Edge mode -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />


  <!-- Typekit -->
  <script src="//use.typekit.net/icq3lxj.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>

</head>

<body>

<div id="container">

  <h1>&nbsp;
    &nbsp;
    &nbsp;</h1>
    <h2>Leerstijlentest emails:</h2>

<?
//encode email - http://davidwalsh.name/php-email-encode-prevent-spam
function encode_email($e) {
  for ($i = 0; $i < strlen($e); $i++) { $output .= '&#'.ord($e[$i]).';'; }
  return $output;
}

//statement
$stm =  $conn->prepare("SELECT DISTINCT email, jaar, richting FROM lt_email, lt_study WHERE lt_email.user_id = lt_study.user_id");
$stm->execute();

$i = 0; //set counter to number results
$c = 0; // set color odd/even

while ($results = $stm->fetch(PDO::FETCH_ASSOC)) {

  $email = encode_email($results['email']);


  $i++;  ?>

<div class="email <?php echo $c++&1 ? 'odd' : 'even'; ?>">

  <p>
  <? echo "<br /> <br /><h5>" . $i . ".</h5> <strong>Email:</strong> " . $email . " <br /><strong>Richting:</strong> " . $results['richting'] . " <br /><strong>Jaar:</strong> " . $results['jaar']; ?>
</p>
</div>





<? } ?>

</div>

</body>
</html>
