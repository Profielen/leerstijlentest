<? error_reporting(E_ALL);
ini_set("display_errors", 1);
################################################################################################################################################################################
########################## CONNECT TO DB ###############################################
########################################################################################
require_once('../conf.inc');


try {
    $conn = new PDO($dsn, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

############################################################################################

?>
<!doctype html>

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>
  <meta charset="utf-8">

  <title>Profielen | Leerstijlentest</title>

  <meta name="description" content="Het onafhankelijk blad en site van de Hogeschool Rotterdam" />
  <meta name="author" content="Profielen" />

  <!-- http://t.co/dKP3o1e -->
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1" />

  <!-- For all browsers -->
  <link href="http://profielen.hr.nl/leerstijlentest/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <link href="http://profielen.hr.nl/leerstijlentest/css/print.css" media="print" rel="stylesheet" type="text/css" />

  <!-- IE9 Edge mode -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />


  <!-- Typekit -->
  <script src="//use.typekit.net/icq3lxj.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>

</head>

<body>

  <div id="container">

<div id="results">

<h1>&nbsp;
  &nbsp;
  &nbsp;</h1>
  <h2>Leerstijlentest data:</h2>

  <? //statement
  $stm =  $conn->prepare("SELECT COUNT(DISTINCT user_id) AS tester, user_id FROM lt_results");
  $stm->execute();



  while ($stats = $stm->fetch(PDO::FETCH_ASSOC)) {

    $timestamp= $stats['user_id'];
    $date = gmdate("d-m-Y", $timestamp);


    ?>

    <p><strong>Aantal mensen die de test hebben gedaan: </strong>
    <? echo $stats['tester']; ?> </p>

    <p><strong>Laatste test gedaan op: </strong>
    <? echo $date; ?> </p>



  <? } ?>

  <p>Lijst met <a href="/leerstijlentest/stats/emails.php">emails</a></p>

</div>

</div></body></html>
