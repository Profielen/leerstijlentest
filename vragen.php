<?php
############################################################################################
########################## CONNECT TO DB ###################################################
############################################################################################
//error_reporting(E_ALL);
require_once('conf.inc');


try {
    $conn = new PDO($dsn, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

############################################################################################

?>


<div id="intro">

		<h1>Wat is jouw persoonlijke leerstijl?</h1>

		<p>Hoe leer jij het liefst? Door kennis toe te passen, door diep in de stof te duiken of door een stappenplan te maken? Doe deze test en kom erachter of je een doener, dromer, denker of beslisser bent. </p>

<div class="kolb">
  <!-- <h5 class="kolb2">Leerstijlen volgens David Kolb</h5> -->
  <p>Volgens de Amerikaanse leerpsycholoog David Kolb moet je vier fasen doorlopen om iets grondig te leren:</p>
  <ol>
  <li>Je ervaart iets nieuws</li>
  <li>Je laat het op je inwerken</li>
  <li>Je analyseert, ziet verbanden en trekt conclusies</li>
  <li>Je past je inzichten voortaan toe</li>
  </ol>
  <p>De fase die jou van nature het makkelijkst afgaat, bepaalt je persoonlijke leerstijl. Dat is de fase waarmee je het liefst begint en waaraan je ook de meeste tijd spendeert. Toch is het belangrijk dat je ook de andere fasen doorloopt. Om effectief te leren, moet je dus extra aandacht besteden aan de leeractiviteiten waarin je je wat minder goed thuis voelt.</p>


</div>

		</div>






<h2 class="stelling">Doe de test</h2>

<div class="clear"> </div>


<form name="leerstijl" id="leerstijl" class="clear" method="post" action="gegevens.php">

<?php //statement
$stm =  $conn->prepare("SELECT * FROM lt_questions ORDER BY rand()");
$stm->execute();

$j = 1; //set counter to number results
$c = 0; // set color odd/even

while ($question = $stm->fetch(PDO::FETCH_ASSOC)) {  ?>


	<div class="question-line">
  <div class ="number <?php echo $c++&1 ? 'odd' : 'even'; ?>"><? echo "<span class='stelling'>Stelling " .$j; ?></span>
  <label for="question"><? echo $question['question']; ?></label>

  <input type="hidden" name="category[<? echo $question['q_id']; ?>]" value="<? echo $question['cat_id']; ?>" />
</div>
</div>


<div class="answers">

           <?php for ($i = 0; $i < 5 ; $i++) {

           if (4-$i == 0) { $Fanswer = "Helemaal oneens"; }
        elseif(4-$i == 1) { $Fanswer = "Gedeeltelijk oneens"; }
        elseif(4-$i == 2) { $Fanswer = "Neutraal"; }
        elseif(4-$i == 3) { $Fanswer = "Gedeeltelijk eens"; }
        elseif(4-$i == 4) { $Fanswer = "Helemaal eens"; }

             ?>

   <div class="radiobox">
      <input type="radio" name="answer[<?php echo $question['q_id']; ?>]" value="<?php echo 4-$i; ?>" required />
      <label for="answer[<?php echo $question['q_id']; ?>]"> <?php echo $Fanswer; ?> </label>

</div>

           <?php } ?>

</div>




<?

      $j++;

    } // end while question ?>


  <hr />

<div class="field">

<h3 class="gegevens">Wat gebeurt er met je gegevens?</h3>
<p>De uitslag van de leerstijlentest wordt niet gekoppeld aan je e-mailadres of de gegevens die je hieronder achterlaat. Je privacy is dus gewaarborgd.
Voor de analyse van de leerstijlentest, willen we graag weten welke studierichting je volgt. Deze vraag is niet verplicht.</p>
<p> Je e-mail wordt eenmalig gebruikt om de resultaten naar je te e-mailen en voor als je onze nieuwsbrief wilt ontvangen.</p>



<label for="studie"> Studierichting</label>
	        <select name="studie">
                <option value=""> </option>
                <option value="Economie">Economie</option>
                <option value="Gedrag & maatschappij">Gedrag & maatschappij</option>
                <option value="Gezondheidszorg">Gezondheidszorg</option>
                <option value="Media & ICT">Media & ICT</option>
                <option value="Kunst">Kunst</option>
                <option value="Onderwijs">Onderwijs</option>
								<option value="Techniek & bouw">Techniek & bouw</option>

        </select>

	</div>

<span class="field">
  <label for="jaar"> Jaar</label>
            <select name="jaar">
                  <option value=""> </option>
                  <option value="1e jaar">1e jaar</option>
                  <option value="2e jaar">2e jaar</option>
                  <option value="3e jaar">3e jaar</option>
                  <option value="4e jaar">4e jaar</option>
                  <option value="5e jaar of hoger">5e jaar of hoger</option>


          </select>

    </span>

<span class="field email">

  <label for="email">E-mail mijn resultaat naar:</label>
	<input type="email" id="email" name="email" placeholder="e-mail" />

<br />
<br />
<label for="newsletter">Ik wil graag de wekelijkse nieuwsbrief van <em>Profielen</em> ontvangen.</label>
<input type="checkbox" class="newsletter" name="newsletter" id="newsletter" />

</span>

<?php
//create unique anonymous identifier for users to keep score in database and avoid double post
$t = time(); //get unix time
?>
<input type="hidden" name="user" value="<? echo "$t"; ?>" />


<span class="controls">
  <input type="submit" value="Ga naar je score" name="submit" id="submit" class="bigbt" />
</span>

	</form>
