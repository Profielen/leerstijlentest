<?php error_reporting(E_ALL); ?>
<!DOCTYPE html>
<html lang="nl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Profielen | Leerstijlentest</title>

<meta name="description" content="Het onafhankelijk blad en site van de Hogeschool Rotterdam" />
<meta name="author" content="Profielen" />

<!-- http://t.co/dKP3o1e -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=1" />


<!-- CSS -->
<link rel="stylesheet" href="<?php echo 'css/screen.css?v=' .  time(); ?>" />

<!-- Javascript


<script src="/leerstijlen/js/webforms2/webforms2-p.js"></script>  -->

<!-- Typekit -->
<script src="//use.typekit.net/icq3lxj.js"></script>
<script>try{Typekit.load();}catch(e){}</script>


<!-- Facebook widget
<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=206393906110803";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	 -->

<!-- Open Graph Tags -->
<meta property="og:type" content="website" />
<meta property="og:title" content="Profielen" />
<meta property="og:description" content="Leerstijlentest" />
<meta property="og:url" content="http://profielen.hr.nl/leerstijlentest/" />
<meta property="og:site_name" content="Profielen" />
<meta property="og:image" content="http://profielen.hr.nl/leerstijlentest/img/Denker.jpg" />
<meta name="twitter:site" content="@profielen" />

<!-- Analytics -->
<script>
/*
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-34695413-2', 'auto');
	ga('send', 'pageview');
	*/

</script>


</head>


<body class="index">

	<div id="logo">
			<a href="http://profielen.hr.nl/leerstijlen/">
		<img src="img/logo.png" alt="logo" />
				</a>
		</div>

	<div id="container">



	<?php include "vragen.php"; ?>


<?php include "footer.php"; ?>
