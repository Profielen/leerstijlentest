# README #

Leerstijlentest ontwikkeld door Bart Siebelink (test) en Profielen (techniek)

* Versie 1.0 gemaakt in 2013
* Versie 2.0 bijgewerkt in 2020


### Functie test ###
* 20 multi choice vragen om te bepalen wat je leerstijl is.
* Optionele info: Studierichting en Jaar
* Mogelijkheid inschrijven voor nieuwsbrief

### Hoe werkt het? ###
* PHP / PDO
* `vragen.php` invullen
* `gegevens.php` data verwerken
* `leerstijl.php` toon resultaat aan de hand van uniek ID (timestamp) van gebruiker

### Probleem ###
Hoe resultaat (Hoogst scorende leerstijl en beeld) mailen naar gebruiker (en extra info zoals social media icons en links)?
Mail functie is op `gegevens.php`. Resultaat word opgehaald in `leerstijl.php`. Gebruiker kan aan de hand van zijn
uniek ID altijd terug kijken op `leerstijl.php`


### Links ###

Originele live test: [Live versie](https://profielen.hr.nl/leerstijlentest/)

Prototype versie 2.0: [Leerstijlen dev versie](https://profielen.hr.nl/lr-test/)

Prototype resultaat pagina na invullen: [De Score](https://profielen.hr.nl/lr-test/leerstijl.php?u=1594729884)
