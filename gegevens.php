<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
############################################################################################
########################## CONNECT TO DB ###################################################
############################################################################################
require_once('conf.inc');


try {
    $conn = new PDO($dsn, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

############################################################################################

include "header.php";

	#####################################################################################################################
	########### Code help by Sitepoint ##################################################################################
	######## http://www.sitepoint.com/forums/showthread.php?1213607-Count-quiz-score-per-category& ######################
	######## http://www.sitepoint.com/forums/showthread.php?1216935-ORDER-BY-variable-array-within-a-foreach-loop #######
	#####################################################################################################################

	if (isset($_POST['submit'])) {

	    $total = array();   //count total score per category

	       foreach($_POST["category"] as $key => $value):

	           if(!array_key_exists($value,$total)): $total[$value] = ""; endif;
	           $total[$value] += $_POST["answer"][$key];

	       endforeach;



	       //Save results
		   arsort($total);// sort categories by total score DESC
	       foreach($total as $cat => $score):



         //get category descriptions || $conn is declared above in database connection
			   $stm =  $conn->prepare("SELECT * FROM lt_category WHERE cat_id = '$cat'");
         $stm->execute();

			   if($stm === FALSE) {

               echo "something went wrong";

			             }

			   else {


             while ($result = $stm->fetch(PDO::FETCH_ASSOC)) { ?>



				<?php
             #############################################
             ##### SAVE DATA IN DATABASE #################
             #############################################


             //create unique anonymous identifier for users to keep score in database
             $user = $_POST["user"]; //timestamp IS unique, no security risk with this test
             //$user = md5($t); //encrypt timestamp->NOT unique

             $catId = $result['cat_id'];

             //add studiejaar en richting

             //if ()

             //insert score into table
             $stmSave = $conn->prepare("INSERT INTO lt_results(user_id, cat_id, score) VALUES (:User,:Cat,:Score)");
             $stmSave->bindParam(':User', $user);
             $stmSave->bindParam(':Cat', $catId);
             $stmSave->bindParam(':Score', $score);
             $stmSave->execute();

        }//end while
			} //end else
			  //$i++;
	       endforeach;

         #############################################
         ##### SAVE DATA IN DATABASE #################
         #############################################

         //check if user submitted jaar and studie
           if ($_POST["jaar"] && $_POST["studie"] ==! NULL) {

          $jaar = $_POST["jaar"];
          $studie = $_POST["studie"];

         //insert jaar and studie
         $stmSave2 = $conn->prepare("INSERT INTO lt_study(user_id, jaar, richting) VALUES (:User,:Jaar,:Studie)");
         $stmSave2->bindParam(':User', $user);
         $stmSave2->bindParam(':Jaar', $jaar);
         $stmSave2->bindParam(':Studie', $studie);
         $stmSave2->execute();

       } //end if jaar en studie



  //check if user submitted email for the result but does NOT want newsletter
  if ($_POST["email"] ==! NULL) {


             //send email
             $to = $_POST["email"];
             $subject = 'profielen leerstijlentest resultaat';
             $message = 'je leerstijlen is dit en dat';
             $from = 'profielen@hr.nl';


             if(mail($to, $subject, $message)) {
                echo 'Your mail has been sent successfully.';
                  } else {
                    echo 'Unable to send email. Please try again.';
                  }



                          //check if user wants newsletter
                          if ($_POST["newsletter"] ==! NULL) {
                            $user = $_POST["user"];
                            $email = $_POST["email"];

                           //insert email
                           $stmSave3 = $conn->prepare("INSERT INTO lt_email(user_id, email) VALUES (:User,:Email)");
                           $stmSave3->bindParam(':User', $user);
                           $stmSave3->bindParam(':Email', $email);
                           $stmSave3->execute();}

           }//end IF


//take user to results page
header("Location: http://profielen.hr.nl/lr-test/leerstijl.php?u=$user");

exit;


 } //end if submit


else { echo "no data"; }


	?>



</div> <!-- end resultaten -->

<?php include "footer.php"; ?>
