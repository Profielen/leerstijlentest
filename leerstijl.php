<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
############################################################################################
########################## CONNECT TO DB ###################################################
############################################################################################
require_once('conf.inc');


try {
    $conn = new PDO($dsn, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

############################################################################################
include "header.php";

?>



<p class="resUitleg">
  Per leerstijl kan je maximaal 20 en minimaal 0 punten halen. De ideale student heeft hoge scores op alle vier de leerstijlen.
  Hoe sterker je daarvan afwijkt, hoe duidelijker het is waar je aan moet werken.</p>



<div id="resultaten">
  <h2>De score</h2>
  <h3 class="uitleg">Jij bent het meest een:</h3>

<?php


   if (isset($_GET['u'])) {
        $user = $_GET['u'];

      $i = 0; //set counter to number results
      //$c = 0; // set color odd/even

       //get score || $conn is declared above in database connection
       $stm =  $conn->prepare("SELECT * FROM lt_results WHERE user_id = '$user'");
       $stm->execute();


           while ($result = $stm->fetch(PDO::FETCH_ASSOC)) {

             $i++;

             $catID = $result['cat_id'];
             $score = $result['score'];

              //get category name and description
             $stmCat =  $conn->prepare("SELECT * FROM lt_category WHERE cat_id = '$catID'");
             $stmCat->execute();

                while ($cRslt = $stmCat->fetch(PDO::FETCH_ASSOC)) {

                         ?>



      <div class="resultaat <? echo $cRslt['name']; ?> <? #echo $c++&1 ? 'odd' : 'even'; ?>">


    <h3 class="category"> <? echo  " " . $i . ". " . $cRslt['name'] . " "; ?> </h3>

  <img src="/lr-test/img/<? echo $cRslt['name']; ?>.jpg" alt=" " class="<? echo $cRslt['name']; ?>"/>

        <p class="score"> <? echo "(score: "  . $score. ")"; ?></p>

  <div class="desc">
     <? echo $cRslt['desc']; ?>
   </div>

          </div>

          <?php
               }
             } ?>
<!--
             <div class="save">Bekijk je resultaat later nog eens op <br />
               <a href="http://profielen.hr.nl/leerstijlentest/leerstijl.php?u=<? echo "$user"; ?>">
                  http://profielen.hr.nl/leerstijlentest/leerstijl.php?u=<? echo "$user"; ?>
                </a>
             </div>
-->


        <?php   } //end if get

           else { echo "Houston, we got a problem"; }
                                ?>
           </div>

<?php include "footer.php"; ?>
